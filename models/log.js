exports.logSchema = {

    user: {
        type: "string"
    },
    message: {
        type: "string"
    },
    status: {
        type: "integer"
    },
    timestamp: {
        type: "timestamp"
    }

}