exports.userSchema = {
    username: {
        type: "string"
    },
    password: {
        type: "string"
    },
    name: {
        type: "array",
        frist_name: {
            type: "string"
        },
        last_name: {
            type: "string"
        }
    },
    token: {
        type: "array",
        web_token: {
            type: "string"
        },
        android_token: {
            type: "string"
        },
        token_expire_time: {
            type: "timestamp"
        }
    },
    created_date: {
        type: "timestamp"

    },
}