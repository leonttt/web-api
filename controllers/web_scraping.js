'use strict'

const request = require('request')
const cheerio = require('cheerio')
var fs = require('fs');
var url = require('url');
const link = '../test/test_search_item.html'
//const link = 'test search item_eng.html'

// const link = 'http://www3.consumer.org.hk/pricewatch/supermarket/index.php?keyword=%E8%8C%B6'
// const link = 'http://www3.consumer.org.hk/pricewatch/supermarket/index.php?keyword=089782044662&lang=en'
// const link = 'http://www3.consumer.org.hk/pricewatch/supermarket/index.php?keyword=089782044662&lang=tc'
//console.log("TEST 0")

exports.search = (req, res, next) => {
  console.log("log : search");
  // console.log(req);
  var keyword, lang
  keyword = req.params.keyword
  lang = req.params.lang
 
  if (lang == undefined) {
    lang = 'tc'
  }else if (lang == 'tc' || lang == 'en'){
    
  }else{
    lang = 'tc'
  }
  const link = `http://www3.consumer.org.hk/pricewatch/supermarket/index.php?keyword=${keyword}&lang=${lang}`



  getData(link).then(function (data) {
    const result = web_scraping(data, lang)
    console.log(result);
    res.send(result)
  }).catch(function (error) {
    console.log({"status": '500' , "message" : "Server Error!"});
    res.send({"status": '500' , "message" : "Server Error!"})
  });

  

};

var url_parts = url.parse(link, true);
var parameter = url_parts.query;



//local - for test web scraping
// console.log(web_scraping(fs.readFileSync(link), parameter.lang))

//Web - live time data

// getData(link).then(function (data) {

//   console.log(web_scraping(data,parameter.lang));
//   console.log("TEST 4")
// }).catch(function (error) {
//   console.log("Failed!", error);
// });



function getData(url) {
  // Setting URL and headers for request
  var options = {
    url: url,
    headers: {
      'User-Agent': 'request'
    }
  };

  // Return new promise
  return new Promise(function (resolve, reject) {
    // Do async job
    request.get(options, function (error, response, html) {
      if (error) {
        reject(error);
      } else {
        resolve(html);
      }
    });
  });
}


function web_scraping(html, lang) {
  // console.log("TEST 2")
  //console.log(lang)
  var item_json = new Array();
  // Setting information
  item_json.push({
    status: "200",
    lang: lang
  });
  var field_array = ['itemcode', 'category', 'brand', 'product', 'wellcome_price', 'parknshop_price', 'market_place_price', 'watsons_price', 'aeon_price', 'dch_food_mart_price', 'last_updated'];
  const $ = cheerio.load(html);
  // Find the data in id="theMainContent" table
  $('#theMainContent').find('table').each(function (index, element) {

    if (index == 1) {

      $(element).find('tr').each(function (index2, element2) {
        var item_Object = new Object(); // Temp save product data

        $(element2).find('td').each(function (index3, element3) {

          if (index2 > 0) {
            // Scraping data from td
            const data = $(element3).text();
            const field = field_array[index3];
            item_Object[field] = data;

            // Other data inside td
            if (index3 == 3) {
              var link = $(element3).find('a').attr('href')
              item_Object.link = link;

            } else if (index3 == 0) {
              var itemcode = $(element3).find('input').attr('value')
              item_Object.itemcode = itemcode;

            }



          }

        });



        // console.log("item_Object")
        // console.log(item_Object)
        // console.log(Object.keys(item_Object).length)

        // Check if item_Object is Null , First tr is not data 
        if (Object.keys(item_Object).length > 0) {
          item_json.push(item_Object);
        }

      });


    }

  });


  var jsonArray = JSON.stringify(item_json)
  return jsonArray
  // console.log(jsonArray)
  // console.log(JSON.parse(jsonArray))
  // console.log(item_json.length)
}