exports.myFavoriteSchema = {

    itemcode: {
        type: "string"
    },
    barcode: {
        type: "string"
    },
    wellcome_price: {
        type: "double",
    },
    parknshop_price: {
        type: "double",
    },
    market_place_price: {
        type: "double",
    },
    watsons_price: {
        type: "double",
    },
    aeon_price: {
        type: "double",
    },
    dch_food_mart_price: {
        type: "double",
    },
    timestamp: {
        type: "timestamp"
    }

}