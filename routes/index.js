var express = require('express');
var router = express.Router();
var user = require('../controllers/user');
var web_scraping = require('../controllers/web_scraping');

/* list*/
router.get('/search/:keyword', web_scraping.search);
router.get('/search/:keyword/:lang', web_scraping.search);

module.exports = router;