exports.productSchema = {

    itemcode: {
        type: "string"
    },
    barcode: {
        type: "string"
    },
    category: {
        type: "array",
        tc: {
            type: "string"
        },
        en: {
            type: "string"
        }

    },
    brand: {
        type: "array",
        tc: {
            type: "string"
        },
        en: {
            type: "string"
        }
    },
    product_name: {
        type: "array",
        tc: {
            type: "string"
        },
        en: {
            type: "string"
        }
    },
    timestamp: {
        type: "timestamp"
    }

}